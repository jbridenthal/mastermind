﻿Feature: UserInput
	To test the output of when a user enters in a key

Scenario: At the end of a game a user does not want to play another game
	Given I have played a game and finished
	When I press N
	Then then that should send me back to the main menu

Scenario: At the end of a game a user wants to play another game
	Given I have played a game and finished
	When I press Y
	Then then that should start a new game

Scenario: At the end of a game a user presses a random key
	Given I have played a game and finished
	When I press K
	Then then that should start a new game