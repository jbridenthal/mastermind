﻿Feature: Getting Random Numbers for Game Code to unlock

Scenario: I should get a four digit number
Given that I request "4" digit game number
Then I should have "4" numbers

Scenario: I should get a five digit number
Given that I request "5" digit game number 
Then I should have "5" numbers

Scenario: I should get a six digit number
Given that I request "6" digit game number 
Then I should have "6" numbers


