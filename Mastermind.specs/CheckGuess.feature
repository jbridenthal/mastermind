﻿Feature: Check Players Guess
Scenario: User enters in guess with zero correct number in the correct order
	Given the winning number is "5656"
	And I have entered a valid guess of "1234" 
	Then the result should be ""

Scenario: User enters in guess with one correct number in the correct order
	Given the winning number is "1565"
	And I have entered a valid guess of "1234"
	Then the result should be "+"

Scenario: User enters in guess with three correct number in the incorrect order with two consecutive numbers the same in the guess
	Given the winning number is "3561"
	And I have entered a valid guess of "1663"
	Then the result should be "+--"

Scenario: User enters in guess with one correct number in the incorrect order
	Given the winning number is "1567"
	And I have entered a valid guess of "2134"
	Then the result should be "-"

Scenario: User enters in guess with two correct numbers in the correct order
	Given the winning number is "1267"
	And I have entered a valid guess of "1234"
	Then the result should be "++"

Scenario: User enters in guess with two correct numbers in the incorrect order
	Given the winning number is "1527"
	And I have entered a valid guess of "1234"
	Then the result should be "+-"

Scenario: User enters in guess with three correct number in the correct order
	Given the winning number is "1237"
	And I have entered a valid guess of "1234"
	Then the result should be "+++"

Scenario: User enters in guess with three correct number in the incorrect order
	Given the winning number is "1523"
	And I have entered a valid guess of "1234"
	Then the result should be "+--"

Scenario: User enters in guess with four correct number in the correct order
	Given the winning number is "1234"
	And I have entered a valid guess of "1234"
	Then the result should be "++++"

Scenario: User enters in guess with four correct number in the incorrect order
	Given the winning number is "1523"
	And I have entered a valid guess of "1235"
	Then the result should be "+---"

Scenario: User enters in guess with one correct number in the correct order with winning number containing two of a single digit
	Given the winning number is "1156"
	And I have entered a valid guess of "1234"
	Then the result should be "+"

Scenario: User enters in guess with two correct numbers in the correct order with winning number containing two of a single digit
	Given the winning number is "1156"
	And I have entered a valid guess of "1124"
	Then the result should be "++"

Scenario: User enters in guess with three correct numbers in the correct order with winning number containing two of a single digit
	Given the winning number is "1152"
	And I have entered a valid guess of "1124"
	Then the result should be "++-"

Scenario: User enters in guess with three correct numbers with a repeating number and with winning number containing two correct digits
	Given the winning number is "1234"
	And I have entered a valid guess of "1134"
	Then the result should be "+++"

Scenario: User enters in guess with three correct numbers with three repeating number and with winning number containing three correct digits in a different order
	Given the winning number is "1131"
	And I have entered a valid guess of "1114"
	Then the result should be "++-"

Scenario: User enters in guess with three correct numbers with a repeating number and with winning number containing one correct digits
	Given the winning number is "1234"
	And I have entered a valid guess of "1114"
	Then the result should be "++"

Scenario: User enters in guess with two correct numbers with a repeating number not consecutive 
	Given the winning number is "1234"
	And I have entered a valid guess of "1514"
	Then the result should be "++"

Scenario: User enters in guess with zero correct number in the correct order for eight
	Given the winning number is "56785678"
	And I have entered a valid guess of "12341234" 
	Then the result should be ""

Scenario: User enters in guess with one correct number in the correct order for eight
	Given the winning number is "16666666"
	And I have entered a valid guess of "12344444"
	Then the result should be "+"

Scenario: User enters in guess with one correct number in the incorrect order for eight
	Given the winning number is "61666666"
	And I have entered a valid guess of "12345555"
	Then the result should be "-"

Scenario: User enters in guess with two correct number in the correct order for eight
	Given the winning number is "12666666"
	And I have entered a valid guess of "12333333"
	Then the result should be "++"

Scenario: User enters in guess with two correct numbers in the incorrect order for eight
	Given the winning number is "15255555"
	And I have entered a valid guess of "16326666"
	Then the result should be "+-"

Scenario: User enters in guess with three correct number in the correct order for eight
	Given the winning number is "12355555"
	And I have entered a valid guess of "12366666"
	Then the result should be "+++"

Scenario: User enters in guess with three correct number in the incorrect order for eight
	Given the winning number is "15266666"
	And I have entered a valid guess of "12345333"
	Then the result should be "+--"

Scenario: User enters in guess with eight correct number in the correct order for eight
	Given the winning number is "12345656"
	And I have entered a valid guess of "12345656"
	Then the result should be "++++++++"

Scenario: User enters in guess with eight correct number in the incorrect order for eight
	Given the winning number is "12345666"
	And I have entered a valid guess of "66654321"
	Then the result should be "--------"

Scenario: User enters in guess with one correct number in the correct order with winning number containing two of a single digit for eight
	Given the winning number is "11565555"
	And I have entered a valid guess of "12343333"
	Then the result should be "+"

Scenario: User enters in guess with two correct numbers in the correct order with winning number containing two of a single digit for eight
	Given the winning number is "11565555"
	And I have entered a valid guess of "11243333"
	Then the result should be "++"

Scenario: User enters in guess with three correct numbers in the correct order with winning number containing two of a single digit for eight
	Given the winning number is "11523333"
	And I have entered a valid guess of "11245555"
	Then the result should be "++--"

Scenario: User enters in guess with three correct numbers with a repeating number and with winning number containing two correct digits for eight
	Given the winning number is "12345555"
	And I have entered a valid guess of "11346666"
	Then the result should be "+++"

Scenario: User enters in guess with three correct numbers with a repeating number and with winning number containing one correct digits for eight
	Given the winning number is "12345666"
	And I have entered a valid guess of "11146555"
	Then the result should be "++--"

Scenario: User enters in guess with two correct numbers with a repeating number not consecutive for eight
	Given the winning number is "15345656"
	And I have entered a valid guess of "16146565"
	Then the result should be "++----"

Scenario: User enters in guess with two correct numbers with two repeating number not consecutive for eight
	Given the winning number is "15316565"
	And I have entered a valid guess of "16365656"
	Then the result should be "++----"

Scenario: User enters in guess with two correct numbers with a three repeating number not consecutive for eight
	Given the winning number is "15345656"
	And I have entered a valid guess of "51416162"
	Then the result should be "-----"

	Scenario: User enters in guess with a letter and it should be invalid
	Given the winning number is "1234"
	And I have entered a guess of "1a14"
	Then it should be invalid

Scenario: User enters in guess with a non alphanumeric character and it should be invalid
	Given the winning number is "1234"
	And I have entered a guess of "1@14"
	Then it should be invalid
	
Scenario: User enters in guess with a letter and a non alphanumeric character and it should be invalid
	Given the winning number is "1234"
	And I have entered a guess of "1a@4"
	Then it should be invalid	
	
Scenario: User enters in guess with less numbers than the winning number
	Given the winning number is "1234"
	And I have entered a guess of "114"
	Then it should be invalid			

Scenario: User enters in guess with more numbers than the winning number
	Given the winning number is "1234"
	And I have entered a guess of "11456"
	Then it should be invalid

Scenario: User enters in no guess
	Given the winning number is "1234"
	And I have entered a guess of ""
	Then it should be invalid

Scenario: User enters in a guess with a single digit greater than 6
	Given the winning number is "1234"
	And I have entered a guess of "1237"
	Then it should be invalid