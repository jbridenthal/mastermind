﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Mastermind.specs.StepDefinitions
{
    [Binding]
    public class GettingRandomNumbersForGameCodeToUnlockSteps
    {
        private int _gameNumber;
        [Given(@"that I request ""(.*)"" digit game number")]
        public void GivenThatIRequestDigitGameNumber(int p0)
        {
            
            _gameNumber = GameCommands.GetGameNumber(p0);
        }

        [Then(@"I should have ""(.*)"" numbers")]
        public void ThenIShouldHaveNumbers(int p0)
        {
            Assert.AreEqual(_gameNumber.ToString().Length, p0);
        }
    }
}
        
