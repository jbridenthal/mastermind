﻿using System;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Mastermind.specs.StepDefinitions
{
    [Binding]
    public class UserInputSteps
    {
        [Given(@"I have played a game and finished")]
        public void GivenIHavePlayedAGameAndFinished()
        {
            //
        }

        [When(@"I press N")]
        public void WhenIPressN()
        {
            //
        }

        [When(@"I press Y")]
        public void WhenIPressY()
        {
            //
        }

        [Then(@"then that should send me back to the main menu")]
        public void ThenThenThatShouldSendMeBackToTheMainMenu()
        {
            var gameCommands = new GameCommands();
            var endOfGame = gameCommands.EndOfGame(ConsoleKey.N);
            Assert.AreEqual(false,endOfGame);
        }

        [Then(@"then that should start a new game")]
        public void ThenThenThatShouldStartANewGame()
        {
            var gameCommands = new GameCommands();
            var endOfGame = gameCommands.EndOfGame(ConsoleKey.Y);
            Assert.AreEqual(true, endOfGame);
        }
        [When(@"I press K")]
        public void WhenIPressK()
        {
            var gameCommands = new GameCommands();
            var endOfGame = gameCommands.EndOfGame(ConsoleKey.K);
            Assert.AreEqual(false, endOfGame);
        }

    }
}