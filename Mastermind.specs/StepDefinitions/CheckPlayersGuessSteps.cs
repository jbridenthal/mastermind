﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Mastermind.specs.StepDefinitions
{
    [Binding]
    public class CheckPlayersGuessSteps
    {
        private int _guessNumber;
        private string _guessNunmerInvalid;
        private int _winningNumber;
        private GameCommands _gameCommands;

        [Given(@"I have entered a valid guess of ""(.*)""")]
        public void GivenIHaveEnteredAValidGuessOf(int guessNumber)
        {
            _gameCommands = new GameCommands();
            _guessNumber = guessNumber;
            Assert.AreEqual(true, _gameCommands.GuessIsAccurate(_guessNumber.ToString(), _winningNumber.ToString().Length));
        }

        [Given(@"the winning number is ""(.*)""")]
        public void GivenTheWinningNumberIs(int winningNumber)
        {
            _winningNumber = winningNumber;
        }

        [Then(@"the result should be ""(.*)""")]
        public void ThenTheResultShouldBe(string correctDisplay)
        {
            _gameCommands = new GameCommands();
            var shouldBeDisplay = _gameCommands.CheckGuessForWin(_guessNumber, _winningNumber);
            Assert.AreEqual(correctDisplay, shouldBeDisplay);

        }
        [Given(@"I have entered a guess of ""(.*)""")]
        public void GivenIHaveEnteredAGuessOf(string guessNumber)
        {
            _guessNunmerInvalid = guessNumber;
        }

        [Then(@"it should be invalid")]
        public void ThenItShouldBeInvalid()
        {
            _gameCommands = new GameCommands();
            Assert.AreEqual(false, _gameCommands.GuessIsAccurate(_guessNunmerInvalid, _winningNumber.ToString().Length));
        }

    }
}