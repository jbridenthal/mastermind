using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastermind
{
    public class GameCommands
    {
        public class Guesses
        {
            public string GuessCorrect;
            public int GuessedNumber;
        }

        public bool FoundWinningNumber { get; set; }
        public int WinningNumberLength { get; set; }
        public int WinningNumber { get; set; }
        public int TotalGuessesAllowed { get; set; }
        public List<Guesses> PreviousGuesses;

        public GameCommands(int totalGuesses, int winningNumberLength)
        {
            TotalGuessesAllowed = totalGuesses;
            WinningNumberLength = winningNumberLength;
            FoundWinningNumber = false;
            PreviousGuesses = new List<Guesses>();
            WinningNumber = GetGameNumber(WinningNumberLength);
        }

        public GameCommands()
        {
            TotalGuessesAllowed = 6;
            WinningNumberLength = 4;
            FoundWinningNumber = false;
            PreviousGuesses = new List<Guesses>();
            WinningNumber = GetGameNumber(WinningNumberLength);
        }

        public static int GetGameNumber(int totalNumberLength)
        {
            //winning number digits must be between 1 and 6
            var gameNumber = -1;
            var rand = new Random();
            for (var i = 0; i < totalNumberLength; i++)
            {
                //If first number then set to a starting number otherwise append the next random number
                gameNumber = gameNumber.Equals(-1)
                    ? rand.Next(1, 6)
                    : Convert.ToInt32(String.Format("{0}{1}", gameNumber, rand.Next(1, 7)));
            }
            return gameNumber;
        }

        public bool GuessIsAccurate(string guess, int lengthOfGuess)
        {
            var guessIsOk = guess.All(Char.IsNumber) && guess.Length.Equals(lengthOfGuess);
            if (guessIsOk)
            {
                foreach (var c in guess)
                {
                    var numberCheck = Convert.ToInt32(c.ToString());
                    if (numberCheck < 1 || numberCheck > 6)
                    {
                        guessIsOk = false;
                    }
                }
            }
            return guessIsOk;
        }

        public string CheckGuessForWin(int guessNumber, int winningNumber)
        {
            var guessCorrect = "";
            //Convert winning number to array. This makes it easier to compare against each individual number
            var winningNumberList = winningNumber.ToString().Select(Convert.ToInt32).ToList();
            var guessNumberList = guessNumber.ToString().Select(Convert.ToInt32).ToList();
            if (winningNumber.Equals(guessNumber))
            {
                FoundWinningNumber = true;
                guessCorrect = guessCorrect.PadRight(guessNumber.ToString().Length, '+');
            }
            else
            {
                FoundWinningNumber = false;
                var winningCount = GetCountsIntoDictionary(winningNumberList);
                var guessCount = GetCountsIntoDictionary(guessNumberList);

                for (var i = 0; i < winningNumberList.Count; i++)
                {
                    //Plus signs always goes in the front, minus signs go in the back as per game rules.
                    if (winningNumberList[i].Equals(guessNumberList[i]))
                    {
                        guessCorrect = guessCorrect.Insert(0, "+");
                    }
                    else
                    {
                        //This checks to see if the number is in the winning number but not in the correct location. It will match it only once.
                        if (winningNumberList.Contains(guessNumberList[i]) && (winningCount[guessNumberList[i]] >= guessCount[guessNumberList[i]]))
                        {
                            guessCorrect = guessCorrect.Insert(guessCorrect.Length, "-");
                        }
                        else
                        {
                            guessCount[guessNumberList[i]]--;
                        }
                    }
                }
            }
            var guess = new Guesses {GuessCorrect = guessCorrect, GuessedNumber = guessNumber};
            PreviousGuesses.Add(guess);
            return guessCorrect;
        }

        private static Dictionary<int, int> GetCountsIntoDictionary(IEnumerable<int> numberList)
        {
            var counts = new Dictionary<int, int>();
            foreach (var value in numberList)
            {
                if (!counts.ContainsKey(value))
                    counts.Add(value, 1);
                else
                    counts[value]++;
            }
            return counts;
        }

        public bool EndOfGame(ConsoleKey key)
        {
            bool endOfGame;
            switch (key)
            {
                case ConsoleKey.N:
                    endOfGame = false;
                    break;
                case ConsoleKey.Y:
                    endOfGame = true;
                    break;
                default:
                    endOfGame = false;
                    break;
            }
            return endOfGame;
        }
    }
}