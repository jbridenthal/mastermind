﻿using System;

namespace Mastermind
{
    public class Program
    {
        private static bool _endProgram;
        private static readonly MenuItems Menu = new MenuItems();
        private static int TotalGuesses { get; set; }
        private static int WinningNumberLength { get; set; } 

        private static void Main()
        {
            //Set default values
            TotalGuesses = 6;
            WinningNumberLength = 4;
            //Run main menu
            while (!_endProgram)
            {
                Console.Clear();
                WriteMainMenuItems();
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.P:
                        PlayGame();
                        break;
                    case ConsoleKey.C:
                        ChangeSettings();
                        break;
                    case ConsoleKey.Escape:
                        _endProgram = true;
                        break;
                }
            }
        }

        private static void ChangeSettings()
        {
            var settingsComplete = true;
            while (settingsComplete)
            {
                Console.Clear();
                WriteConfigurationMenu();
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.G:
                        Console.Clear();
                        Console.WriteLine("Please enter how many times a player can make a guess:");
                        TotalGuesses = Convert.ToInt32(Console.ReadLine());
                        break;
                    case ConsoleKey.N:
                        Console.Clear();
                        Console.WriteLine("Please enter how many numbers should be in the code:");
                        WinningNumberLength = Convert.ToInt32(Console.ReadLine());
                        break;
                    case ConsoleKey.Escape:
                        settingsComplete = false;
                        break;
                }
            }
        }

        private static void PlayGame()
        {
            var playGame = true;
            while (playGame)
            {
                var gameCommands = new GameCommands(TotalGuesses, WinningNumberLength);
                RunSingleGame(gameCommands);
                Console.WriteLine("Would you like to play again? Y/N");
                playGame = gameCommands.EndOfGame(Console.ReadKey(true).Key);
            }
        }

        private static void RunSingleGame(GameCommands gameCommands)
        {
            var guesses = 0;
            var gameOver = false;
            string guessErrror = null;
            while (!gameOver)
            {
                Console.Clear(); //each time we loop we want to clear the console to get a refresh.
                if (guesses < gameCommands.TotalGuessesAllowed)
                {
                    Console.WriteLine("Please enter in a {0} digit code.", gameCommands.WinningNumberLength);
                    Console.WriteLine("All {0} digits will be between 1 and 6.", gameCommands.WinningNumberLength);
                    WritePreviousGuesses(gameCommands);
                   
                    if (guessErrror != null)
                    {
                        Console.WriteLine(guessErrror);
                        guessErrror = null;
                    } 
                    Console.WriteLine("You are on guess {0} of {1}:",guesses + 1, gameCommands.TotalGuessesAllowed);
                    var guess = Console.ReadLine();
                    if (gameCommands.GuessIsAccurate(guess, gameCommands.WinningNumberLength))
                    {
                        gameCommands.CheckGuessForWin(Convert.ToInt32(guess), gameCommands.WinningNumber);
                        gameOver = gameCommands.FoundWinningNumber;
                        guesses++;
                    }
                    else
                    {   //store error to write when console refreshes
                        guessErrror =String.Format("Incorrect Guess!{0}Guess needs to be numbers only,at least {1} long and between 1 and 6. {0}Please enter a new guess!",Environment.NewLine,gameCommands.WinningNumberLength);
                    }
                }
                else
                {//out of guesses, exit loop 
                    gameOver = true;
                }
            }
            Console.Clear();
            Console.WriteLine(gameCommands.FoundWinningNumber ? "You Solved it!" : "You lose :(");
            WritePreviousGuesses(gameCommands); //print all the guesses
            if (!gameCommands.FoundWinningNumber)
            {
                Console.WriteLine("Winning Code: {0}", gameCommands.WinningNumber);
                Console.WriteLine("----------------");
            }
        }

        private static void WritePreviousGuesses(GameCommands gameCommands)
        {
            if (gameCommands.PreviousGuesses.Count > 0)
            {
                Console.WriteLine("----------------");
                Console.WriteLine("Previous Guesses");
                foreach (var previousGuess in gameCommands.PreviousGuesses)
                {
                    Console.WriteLine("{0} {1}", previousGuess.GuessedNumber, previousGuess.GuessCorrect);
                }
                Console.WriteLine("----------------");
            }
     
        }

        private static void WriteMainMenuItems()
        {
            foreach (var menuItems in Menu.MainMenu)
            {
                Console.WriteLine(menuItems);
            }
        }

        private static void WriteConfigurationMenu()
        {
            foreach (var menuItems in Menu.ConfigurationMenu)
            {
                Console.WriteLine(menuItems);
            }
        }
    }
}