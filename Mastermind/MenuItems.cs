﻿using System.Collections.Generic;

namespace Mastermind
{
    internal class MenuItems
    {
        public List<string> MainMenu = new List<string>();
        public List<string> ConfigurationMenu = new List<string>();

        public MenuItems()
        {
            MainMenu.Add("Welcome to MasterMind!");
            MainMenu.Add("Press and option to begin:");
            MainMenu.Add("P to play the game");
            MainMenu.Add("C to configure the game");
            MainMenu.Add("Press Escape to close application");

            ConfigurationMenu.Add("Configure Game Menu");
            ConfigurationMenu.Add("Press and option to change the value:");
            ConfigurationMenu.Add("G to change total number of guesses");
            ConfigurationMenu.Add("N to change how many numbers are in the code");
            ConfigurationMenu.Add("Press Escape to return to previous menu");
        }
    }
}